#pragma once

#include "SDL_gpu.h"
#include "Vec2.h"
#include <string>
#include <vector>

struct Tile
{
public:
	GPU_Rect bounds;
	int x, y;
};

class TileRenderer
{
public:
	Vec2 offset;
	int cellSize;

	GPU_Image* tilesheet;
	std::vector<Tile> tiles;

	TileRenderer();

	bool Load(const std::string& levelFile);

	void Draw(GPU_Target* screen);
};