#pragma once

#include "Sprite.h"

class SpriteObject
{
public:
	Sprite sprite;

	int cellSize;
	Vec2 offset;
	int x, y;
	float scale;

	SpriteObject()
	{
		cellSize = 32;
		x = 0;
		y = 0;
		scale = 1.0f;
	}

	virtual void Update(float dt)
	{}

	virtual void Move(int dx, int dy)
	{
		x += dx;
		y += dy;
	}

	virtual void Draw(GPU_Target* screen)
	{
		sprite.DrawCellScale(screen, cellSize, offset, x, y, scale);
	}
};