#pragma once

#include "SDL_gpu.h"

class GameScreen
{
public:

	virtual void Click(int x, int y)
	{}

	virtual void Update(float dt)
	{}

	virtual void Draw(GPU_Target* screen) = 0;

	virtual bool Close()
	{
		return false;
	}
};