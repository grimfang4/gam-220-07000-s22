#pragma once


#include "GameScreen.h"
#include "Button.h"
#include "Character.h"

class GameData;

class CombatScreen : public GameScreen
{
public:
	GameData& gameData;
	Button attackButton;
	Button runButton;

	bool playersTurn;
	int playerHP;
	int enemyHP;

	CombatScreen(GameData& gameData, Character* player, Character* enemy);

	virtual void Click(int x, int y) override;

	virtual void Update(float dt) override;

	virtual void Draw(GPU_Target* screen) override;
};