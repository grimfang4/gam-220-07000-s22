#include "MenuScreen.h"
#include "GameData.h"

MenuScreen::MenuScreen(GameData& gameData)
	: gameData(gameData)
{
	quitButton.text = "Quit";
	quitButton.bounds = GPU_Rect{ gameData.screen->w / 2.0f - 50, gameData.screen->h / 2 + 200.0f - 60, 100.0f, 50.0f };
	quitButton.font = &gameData.uiFont;
	quitButton.sprite.Set(gameData.uiSpriteSheet1, GPU_Rect{ 190.0f, 45.0f, 190.0f, 49.0f });
}

void MenuScreen::Click(int x, int y)
{
	if (quitButton.InBounds(x, y))
	{
		gameData.done = true;
	}
}

void MenuScreen::Update(float dt)
{}

void MenuScreen::Draw(GPU_Target* screen)
{
	// Menu (opens with spacebar)
	GPU_Rect menuPanel = { screen->w / 2 - 200, screen->h / 2 - 200, 400, 400 };
	GPU_RectangleRoundFilled2(screen, menuPanel, 10, GPU_MakeColor(100, 100, 100, 220));
	GPU_RectangleRound2(screen, menuPanel, 10, GPU_MakeColor(0, 0, 0, 255));
	quitButton.Draw(screen);
}

bool MenuScreen::Close()
{
	return true;
}