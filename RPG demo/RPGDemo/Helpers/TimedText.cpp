#include "TimedText.h"

TimedText::TimedText(float newX, float newY, float newLifetime, NFont* newFont, string newText)
{
	x = newX;
	y = newY;
	lifetime = newLifetime;
	maxLifetime = newLifetime;
	font = newFont;
	text = newText;
	color = GPU_MakeColor(0, 0, 0, 255);
}

void TimedText::Update(float dt)
{
	lifetime -= dt;
}

void TimedText::Draw(GPU_Target* target)
{
	float lifetimeLerp = (lifetime / maxLifetime);
	float height = 50*(1.0f - lifetimeLerp);
	font->draw(target, x, y - height, NFont::Effect(NFont::CENTER, NFont::Color(color.r, color.g, color.b, color.a * lifetimeLerp)), "%s", text.c_str());
}