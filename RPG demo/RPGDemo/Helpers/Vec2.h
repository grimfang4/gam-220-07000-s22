#pragma once


#include <cmath>

class Vec2
{
public:
    float x, y;

    Vec2()
        : x(0.0f), y(0.0f)
    {}
    Vec2(float x, float y)
        : x(x), y(y)
    {}

    Vec2 operator+(const Vec2& other) const
    {
        return Vec2(x + other.x, y + other.y);
    }
    Vec2 operator-(const Vec2& other) const
    {
        return Vec2(x - other.x, y - other.y);
    }
    Vec2& operator+=(const Vec2& other)
    {
        x += other.x;
        y += other.y;
        return *this;
    }
    Vec2& operator-=(const Vec2& other)
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }
    Vec2& operator/=(float divisor)
    {
        x /= divisor;
        y /= divisor;
        return *this;
    }
    Vec2 operator*(float scale) const
    {
        return Vec2(scale*x, scale*y);
    }

    friend Vec2 operator*(float scale, const Vec2& v)
    {
        return Vec2(scale*v.x, scale*v.y);
    }

    float dot(const Vec2& other) const
    {
        return x * other.x + y * other.y;
    }

    static float dot(const Vec2& a, const Vec2& b)
    {
        return a.x * b.x + a.y * b.y;
    }

    float Magnitude() const
    {
        return sqrtf(x*x + y * y);
    }

    float SqrMagnitude() const
    {
        return x * x + y * y;
    }

    void Normalize()
    {
        *this /= Magnitude();
    }
};