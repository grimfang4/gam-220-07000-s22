#pragma once
#include "SDL_gpu.h"
#include <map>
#include <string>
using namespace std;

class ImageManager
{
public:
	map<string, GPU_Image*> images;

	~ImageManager()
	{
		for (auto e = images.begin(); e != images.end(); ++e)
		{
			GPU_FreeImage(e->second);
		}
	}

	GPU_Image* Load(string filename)
	{
		map<string, GPU_Image*>::iterator e = images.find(filename);

		if (e != images.end())
		{
			return e->second;
		}
		else
		{
			//load image
			GPU_Image* image = GPU_LoadImage(filename.c_str());
			//store in map
			images.insert(make_pair(filename, image));
			return image;
		}
	}

	void Unload(string key)
	{
		auto e = images.find(key);
		if (e != images.end())
		{
			GPU_FreeImage(e->second);
			images.erase(e);
		}

	}

	void Free()
	{
		for (auto e = images.begin(); e != images.end(); ++e)
		{
			GPU_FreeImage(e->second);
		}
		images.clear();
	}


};