#pragma once

#include "Button.h"
#include "GameScreen.h"

class GameData;

class MenuScreen : public GameScreen
{
public:
	GameData& gameData;
	Button quitButton;

	MenuScreen(GameData& gameData);

	virtual void Click(int x, int y) override;

	virtual void Update(float dt) override;

	virtual void Draw(GPU_Target* screen) override;

	virtual bool Close() override;
};