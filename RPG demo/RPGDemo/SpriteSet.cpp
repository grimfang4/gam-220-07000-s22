#include "SpriteSet.h"
using namespace std;


SpriteSet::SpriteSet()
	: current(make_pair("", 0))
{

}

void SpriteSet::Draw(GPU_Target* screen, float x, float y) const
{
	auto e = sprites.find(current);
	if (e != sprites.end())
		e->second.Draw(screen, x, y);
}

void SpriteSet::DrawCell(GPU_Target* screen, int cellSize, const Vec2& offset, int x, int y) const
{
	auto e = sprites.find(current);
	if (e != sprites.end())
		e->second.DrawCell(screen, cellSize, offset, x, y);
}