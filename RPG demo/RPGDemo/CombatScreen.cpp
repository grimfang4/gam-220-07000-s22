#include "CombatScreen.h"
#include "GameData.h"


CombatScreen::CombatScreen(GameData& gameData, Character* player, Character* enemy)
	: gameData(gameData)
{
	playersTurn = true;
	// TODO: Get stuff from the actual player and enemy
	playerHP = gameData.rnd.Integer(4, 10);
	enemyHP = gameData.rnd.Integer(4, 10);

	printf("Starting combat.  HP: %d vs %d\n", playerHP, enemyHP);

	attackButton.text = "Attack";
	attackButton.bounds = GPU_Rect{ gameData.screen->w / 2.0f - 50, gameData.screen->h / 2 + 200.0f - 60, 100.0f, 50.0f };
	attackButton.font = &gameData.uiFont;
	attackButton.sprite.Set(gameData.uiSpriteSheet1, GPU_Rect{ 190.0f, 45.0f, 190.0f, 49.0f });


	runButton.text = "Run";
	runButton.bounds = GPU_Rect{ gameData.screen->w / 2.0f - 50, gameData.screen->h / 2 + 300.0f - 60, 100.0f, 50.0f };
	runButton.font = &gameData.uiFont;
	runButton.sprite.Set(gameData.uiSpriteSheet1, GPU_Rect{ 190.0f, 45.0f, 190.0f, 49.0f });
}

void CombatScreen::Click(int x, int y)
{
	if (playersTurn)
	{
		if (attackButton.InBounds(x, y))
		{
			playersTurn = !playersTurn;

			enemyHP--;
			GPU_Log("Player attacks!  Enemy has %d hitpoints left.\n", enemyHP);
			
			if (enemyHP <= 0)
			{
				GPU_Log("You win!\n");
				// Technically safe, but be careful!  Easy to make unsafe.
				gameData.currentScreen = nullptr;
				delete this;
				return;
			}
			return;
		}
		if (runButton.InBounds(x, y))
		{
			playersTurn = !playersTurn;

			if (gameData.rnd.Integer(0, 100) > 50)
			{
				GPU_Log("Ran away!\n");
				// Technically safe, but be careful!  Easy to make unsafe.
				gameData.currentScreen = nullptr;
				delete this;
				return;
			}
			else
				GPU_Log("Failed to run away...\n");
		}
	}
}

void CombatScreen::Update(float dt)
{
	if (!playersTurn)
	{
		playerHP--;
		GPU_Log("Enemy hit player.  Player has %d hitpoints left.\n", playerHP);
		playersTurn = true;

		if (playerHP <= 0)
		{
			GPU_Log("YOU LOSE!\n");

			// Technically safe, but be careful!  Easy to make unsafe.
			gameData.currentScreen = nullptr;
			delete this;
			return;
		}
	}
}

void CombatScreen::Draw(GPU_Target* screen)
{
	GPU_Rect menuPanel = { 20, 20, screen->w - 40, screen->h - 40 };
	GPU_RectangleRoundFilled2(screen, menuPanel, 10, GPU_MakeColor(255, 255, 255, 255));
	GPU_RectangleRound2(screen, menuPanel, 10, GPU_MakeColor(0, 0, 0, 255));
	attackButton.Draw(screen);
	runButton.Draw(screen);
}