#pragma once

#include "SDL_gpu.h"
#include "Sprite.h"
#include "NFont_gpu.h"
#include <string>

class Button
{
public:
	GPU_Rect bounds;
	Sprite sprite;

	std::string text;
	NFont* font;

	Button()
	{
		bounds = {0,0,0,0};
		font = nullptr;
	}

	bool InBounds(float x, float y)
	{
		return (bounds.x <= x && x <= bounds.x + bounds.w && bounds.y <= y && y <= bounds.y + bounds.h);
	}

	void Draw(GPU_Target* screen)
	{
		sprite.DrawBounds(screen, bounds);
		if (font != nullptr)
			font->draw(screen, bounds.x + bounds.w / 2, bounds.y + bounds.h / 2 - font->getHeight("%s", text.c_str()) / 2, NFont::AlignEnum::CENTER, "%s", text.c_str());
	}
};