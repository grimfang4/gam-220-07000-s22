#include "TileRenderer.h"
#include <fstream>
using namespace std;

static vector<string> explodeString(const string& str, char delimiter, bool ignore_empty_chunks = false)
{
	vector<string> result;

	size_t oldPos = 0;
	size_t pos = str.find_first_of(delimiter);
	while (pos != string::npos)
	{
		if (!ignore_empty_chunks || pos - oldPos > 0)
			result.push_back(str.substr(oldPos, pos - oldPos));
		oldPos = pos + 1;
		pos = str.find_first_of(delimiter, oldPos);
	}

	result.push_back(str.substr(oldPos, string::npos));

	return result;
}

TileRenderer::TileRenderer()
{
	tilesheet = nullptr;
	cellSize = 32;
}

bool TileRenderer::Load(const std::string& levelFile)
{
	ifstream fin(levelFile);

	if (fin.bad())
		return false;

	string line;
	
	// First line: The tilesheet image file
	getline(fin, line);
	tilesheet = GPU_LoadImage(line.c_str());

	int sheetCols = tilesheet->w / cellSize;
	int sheetRows = tilesheet->h / cellSize;

	int j = 0;
	while (!fin.eof())
	{
		getline(fin, line);
		if (line.size() == 0)
			continue;
		vector<string> entries = explodeString(line, '|');

		for (int i = 0; i < entries.size(); ++i)
		{
			// |x,y|
			vector<string> pos = explodeString(entries[i], ',');
			int x = stoi(pos[0]);
			int y = stoi(pos[1]);

			Tile t;
			t.bounds = { (float)x * cellSize, (float)y * cellSize, (float)cellSize, (float)cellSize };
			t.x = i;
			t.y = j;
			tiles.push_back(t);
		}
		++j;
	}

	return true;
}

void TileRenderer::Draw(GPU_Target* screen)
{
	for (int i = 0; i < tiles.size(); ++i)
	{
		GPU_Rect dest = tiles[i].bounds;
		dest.x = offset.x + tiles[i].x * cellSize;
		dest.y = offset.y + tiles[i].y * cellSize;

		GPU_BlitRect(tilesheet, &tiles[i].bounds, screen, &dest);
	}
}