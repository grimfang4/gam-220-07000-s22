#pragma once

#include "Sprite.h"
#include <map>
#include <vector>
#include <string>

class SpriteSet
{
public:
	// Maps a string-int pair to a sprite, e.g.
	// ("WarriorRight", 0) -> Warrior looking right, first frame
	// ("WarriorRight", 1) -> Warrior looking right, second frame
	std::map<std::pair<std::string, int>, Sprite> sprites;
	std::pair<std::string, int> current;

	SpriteSet();

	void Draw(GPU_Target* screen, float x, float y) const;
	void DrawCell(GPU_Target* screen, int cellSize, const Vec2& offset, int x, int y) const;
};