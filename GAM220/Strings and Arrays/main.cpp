#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	int myNumbers[] = {389, 2299, 7654, 5};
	int myNumbers2[5] = {890, 34, 2, 364, 33};
	int myNumbersSize = 4;

	for (int i = 0; i < myNumbersSize; ++i)
	{
		cout << myNumbers[i] << endl;
	}

	cout << "Array: " << myNumbers << endl;

	string myString = "AGHJKL";
	char myCharacterArray[] = {'a', 'b', 'c', '\0'};
	char myOtherString[] = "Hey!";
	const char* myLastString = "Hey!";  // Pointer to a character

	cout << "myString: " << myString << endl;
	cout << "myCharacterArray: " << myCharacterArray << endl;
	cout << "myLastString: " << myLastString << endl;

	cout << "Should be y: " << myLastString[2] << endl;

	cout << "myString length: " << myString.size() << endl;

	// Allocate memory with `new`
	int* myIntegers = new int[10];
	// ...
	delete[] myIntegers;

	int* mySpecialInt = new int();
	// ...
	delete mySpecialInt;


	string words[] = {"Apple", "Bacon", "Cheddar"};

	/*char currentChar = 'a';
	for (int i = 0; i < 3; ++i)
	{
		cout << currentChar++ << " is for " << words[i] << endl;
	}*/

	/*for (int i = 0; i < 3; ++i)
	{
		cout << char('a' + i) << " is for " << words[i] << endl;
	}*/

	for (char c = 'a'; c <= 'c'; ++c)
	{
		cout << c << " is for " << words[c - 'a'] << endl;
	}

	while (false)  // 0 is false, 1 (or anything else) is true
	{

	}

	cin.get();

	return 0;
}