#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>
using namespace std;

/* C# with Unity
Vector3 a = new Vector3();
a.x = 5f;
a.Normalize();

a = Vector3.zero;
float d = Vector3.Dot(a, b);
*/

/* C++ with pretend Unity
Vector3 a;
a.x = 5.0f;
a.Normalize();

a = Vector3::zero;
float d = Vector3::Dot(a, b);

*/

class MyClass
{
public:
	static int zero;
	int x;
};

int main(int argc, char* argv[])
{
	cout << endl << "Vector" << endl;
	// Vectors are extensible arrays
	// [0, 1, 2, 3, 4]
	// [0, 1, 3, 4], 4
	// [0, 1, 3, 4]
	vector<string> names;
	names.push_back("Harold");
	names.push_back("Jeremy");
	names.push_back("Richard");
	names.push_back("Hobbes");
	names.push_back("Garfield");

	cout << names[3] << endl;

	for (int i = 0; i < names.size(); ++i)
	{
		cout << names[i] << " ";
	}
	cout << endl;

	names.erase(names.begin() + 2);


	cout << endl << "List" << endl;
	// Linked list is a chain of connected elements
	// [0] -> [1] -> [2] -> [3] -> [4]
	list<string> listNames;
	listNames.push_back("Harold");
	listNames.push_back("Jeremy");
	listNames.push_back("Richard");
	listNames.push_back("Hobbes");
	listNames.push_back("Garfield");
	listNames.push_front("Garfield");

	for (list<string>::iterator e = listNames.begin(); e != listNames.end(); ++e)
	{
		cout << *e << " ";
	}
	cout << endl;

	/*for (vector<string>::iterator e = names.begin(); e != names.end(); ++e)
	{
		cout << *e << " ";
	}
	cout << endl;*/


	cout << endl << "Set" << endl;
	// Set is a non-linear container of unique elements, like a classification
	set<string> mySet;
	mySet.insert("Harold");
	mySet.insert("Jeremy");
	mySet.insert("Richard");
	mySet.insert("Hobbes");
	mySet.insert("Garfield");
	mySet.insert("Garfield");

	for (set<string>::iterator e = mySet.begin(); e != mySet.end(); ++e)
	{
		cout << *e << " ";
	}
	cout << endl;

	if (mySet.find("Jeremy") != mySet.end())
	{
		cout << "Jeremy, I found you!" << endl;
	}
	else
	{
		cout << "Jeremy is not in this set?!" << endl;
	}


	cout << endl << "Map" << endl;
	// Maps are C# Dictionaries...  "associative arrays" mapping a key to a value.
	map<string, string> myDictionary;
	myDictionary.insert(make_pair("shoe", "A specific kind of footwear."));
	myDictionary.insert(make_pair("water bottle", "A container meant to contain water."));


	cout << "I am wearing " << myDictionary["shoe"] << endl;
	cout << "I am under " << myDictionary["table"] << endl;

	myDictionary["water"] = "Some kind of liquid.";

	map<string, string>::iterator foundEntry = myDictionary.find("water bottle");
	if (foundEntry != myDictionary.end())
	{
		cout << "Found it! " << endl;
	}

	auto i = 5;  // let the compiler deduce the type

	//for (map<string, string>::iterator e = myDictionary.begin(); e != myDictionary.end(); ++e)
	for (auto e = myDictionary.begin(); e != myDictionary.end(); ++e)
	{
		pair<string, string> p = *e;
		cout << p.first << ": " << p.second << endl;
	}

	// range-based 'for' loop
	for (auto& s : names)
	{
		cout << s << " ";
	}
	cout << endl;


	return 0;
}