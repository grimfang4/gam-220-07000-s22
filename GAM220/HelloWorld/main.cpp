/*using System;

class Program
{
	public static void Main(string[] args)
	{
		Console.WriteLine("Hello world!");
		Console.ReadLine();
	}
}*/

#include <iostream>  // gives us access to input and output objects
using namespace std;  // I don't have to type std:: in front of things from iostream

// Returns a value indicating exit success or not
int main(int argc, char* argv[])  // Has arguments for the command line
{
	int n = 6;
	n++;
	n += 43;

	cout << "Hello world!" << n << ", " << 678909 << endl;
	cout << "SDJHKLASJ";  // Put some stuff on the same line
	cout << "Some\nmore\ntext.\n";

	cin.get();  // Wait until the user presses enter (but...  details!)

	return 0;
}
