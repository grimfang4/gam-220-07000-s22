#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Enemy
{};

class Weapon
{
public:
	int damage;
	string description;

	virtual void Attack(Enemy& enemy)
	{
		cout << "Attacking this enemy..." << endl;
	}

	virtual void AltAttack()
	{}

	virtual void Throw() = 0;  // Pure virtual function.
	// Must be overridden by a derived class or else that class is also abstract.

	virtual void Block() = 0;
	virtual void Break() = 0;
};

class BladedWeapon : public Weapon
{
	
};

class Sword : public BladedWeapon
{
public:

	virtual void Attack(Enemy& enemy) override
	{
		cout << "Swording this enemy..." << endl;
	}

	virtual void AltAttack() override
	{
		cout << "Sword AltAttack()" << endl;
	}

	virtual void Throw() override
	{
		cout << "You fling the sword..." << endl;
	}

	virtual void Block() override
	{}
	virtual void Break() override
	{}
};

class ThrowingKnife : public BladedWeapon
{
public:
	virtual void Attack(Enemy& enemy) override
	{
		cout << "Knifing this enemy..." << endl;
	}

	virtual void AltAttack() override
	{
		cout << "ThrowingKnife AltAttack()" << endl;
	}

	virtual void Throw() override
	{
		cout << "You throw the knife..." << endl;
	}

	virtual void Block() override
	{}
	virtual void Break() override
	{}
};


int main(int argc, char* argv[])
{
	Enemy enemy;
	//Weapon myWeapon;
	Sword mySword;

	mySword.AltAttack();

	ThrowingKnife myKnife;

	Weapon* myWeapon;
	myWeapon = &mySword;

	myWeapon->Attack(enemy);

	myWeapon = &myKnife;
	myWeapon->Attack(enemy);

	vector<Weapon*> weapons;
	weapons.push_back(&mySword);
	weapons.push_back(&myKnife);
	weapons.push_back(new ThrowingKnife());

	for (auto w : weapons)
	{
		w->Throw();
	}



	return 0;
}